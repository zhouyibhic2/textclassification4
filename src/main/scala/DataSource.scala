package com.bigtester

import java.io.File

import io.prediction.controller.PDataSource
import io.prediction.controller.EmptyEvaluationInfo
import io.prediction.controller.EmptyActualResult
import io.prediction.controller.Params
import io.prediction.controller.SanityCheck
import io.prediction.data.storage.Event
import io.prediction.data.store.PEventStore
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import grizzled.slf4j.Logger
import org.apache.commons.io.FileUtils
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.feature.StringIndexer
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types.{DoubleType, StringType, StructField, StructType}

/** Define Data Source parameters.
  * appName is the application name.
  * evalK is the the number of folds that are to be used for cross validation (optional)
  */
case class DataSourceParams(
    appName: String,
    evalK: Option[Int]
  ) extends Params


/** Define your DataSource component. Remember, you must
  * implement a readTraining method, and, optionally, a
  * readEval method.
  */
class DataSource (
  val dsp : DataSourceParams
) extends PDataSource[TrainingData, EmptyEvaluationInfo, Query, ActualResult] {

  @transient lazy val logger = Logger[this.type]
  def getDistinctCount(df: DataFrame, attrName: String): Long = {
    val attr1s = df.select(attrName)
    attr1s.distinct.count()

  }

  def writeDFLabelIndex(indexed: DataFrame, sqlContext: org.apache.spark.sql.SQLContext, labelString: String) = {

    val dictionaryDfAttr1 = indexed.select(labelString, labelString+"_index")
    val dict1FolderName = "data/"+labelString+"dict.csv"
    FileUtils.deleteDirectory(new File(dict1FolderName))
    dictionaryDfAttr1.distinct.sort(labelString+"_index").write.format("com.databricks.spark.csv").save(dict1FolderName)
    dictionaryDfAttr1.distinct.show(dictionaryDfAttr1.count().toInt)
    val customSchema = StructType(Array(
      StructField("C0", StringType, true),
      StructField("C1", DoubleType, true)
    ))

    val attrScreenWidthread = sqlContext.read
      .format("com.databricks.spark.csv")
      .schema(customSchema)
      .load(dict1FolderName)
    val result = attrScreenWidthread.filter("C1 = 0.0")
    val indexOfTheQ = result.select("C0").first().get(0)
    result.show()
    Console.println("test: " + indexOfTheQ)
    dictionaryDfAttr1.distinct.sort(labelString+"_index").show()

  }

  def transformToRddObservation(sc: SparkContext, dbDf: DataFrame, stringColumns: Array[String]): RDD[Observation] = {
    //var attr1DistinctCount = getDistinctCount(dbDf, stringColumns(0))

    var recordsDf = dbDf
    val sqlContext= new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._

    val index_transformers: Array[org.apache.spark.ml.PipelineStage] = stringColumns.map(
      cname => new StringIndexer()
        .setInputCol(cname)
        .setOutputCol(s"${cname}_index")
    )

    // Add the rest of your pipeline like VectorAssembler and algorithm
    val index_pipeline = new Pipeline().setStages(index_transformers)
    val index_model = index_pipeline.fit(recordsDf)
    var indexed = index_model.transform(recordsDf)

    writeDFLabelIndex(indexed, sqlContext, stringColumns(0))


    //val indexed2 = indexed.drop(stringColumns(0))
    import org.apache.spark.sql.functions._
    if (indexed.count()<1000)
      indexed.sort(desc("phrase")).show(indexed.count.toInt)
    else
      indexed.sort(desc("phrase")).show()
    //val labeledPoints = indexed2.map(row => LabeledPoint(row.getDouble(0), row(4).asInstanceOf[Vector]))
    indexed.map{
      row =>
        val label : Double = row.getDouble(2)
        Observation(
          label,
          row.getString(1),
          row.getString(0)
        )
    }.cache()
  }
  case class Record(Interest: String, phrase: String)
  /** Helper function used to store data given a SparkContext. */
  private def readEventData(sc: SparkContext) : RDD[Observation] = {
    //Get RDD of Events.
    val events: RDD[Event] = PEventStore.find(
      appName = dsp.appName,
      entityType = Some("phrase"), // specify data entity type
      eventNames = Some(List("$set")) // specify data event name

      // Convert collected RDD of events to and RDD of Observation
      // objects.
    )(sc)

    val records: RDD[Record] = events.map(e=>{
      try {
        Record(
          e.properties.get[String]("Interest"),
          e.properties.get[String]("phrase")


        )
      } catch {
        case e: Exception => {
          logger.error(s"Failed to get properties. Exception: ${e}.")
          throw e
        }
      }
    })
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._
    val recordDf =  records.toDF()


    /*events.map(e => {
      val label : String = e.properties.get[String]("Interest")
      Observation(
        if (label == "spam") 1.0 else 0.0,
        e.properties.get[String]("phrase"),
        label
      )
    }).cache*/
    val stringColumns = Array("Interest")
    transformToRddObservation(sc, recordDf, stringColumns)
  }

  /** Helper function used to store stop words from event server. */
  private def readStopWords(sc : SparkContext) : Set[String] = {
    PEventStore.find(
      appName = dsp.appName,
      entityType = Some("resource"),
      eventNames = Some(List("stopwords"))

      //Convert collected RDD of strings to a string set.
    )(sc)
      .map(e => e.properties.get[String]("word"))
      .collect
      .toSet
  }

  /** Read in data and stop words from event server
    * and store them in a TrainingData instance.
    */
  override
  def readTraining(sc: SparkContext): TrainingData = {
    new TrainingData(readEventData(sc), readStopWords(sc))
  }

  /** Used for evaluation: reads in event data and creates cross-validation folds. */
  override
  def readEval(sc: SparkContext):
  Seq[(TrainingData, EmptyEvaluationInfo, RDD[(Query, ActualResult)])] = {
    // Zip your RDD of events read from the server with indices
    // for the purposes of creating our folds.
    val data = readEventData(sc).zipWithIndex()
    // Create cross validation folds by partitioning indices
    // based on their index value modulo the number of folds.
    (0 until dsp.evalK.get).map { k =>
      // Prepare training data for fold.
      val train = new TrainingData(
        data.filter(_._2 % dsp.evalK.get != k).map(_._1),
        readStopWords
          ((sc)))

      // Prepare test data for fold.
      val test = data.filter(_._2 % dsp.evalK.get == k)
        .map(_._1)
        .map(e => (new Query(e.text), new ActualResult(e.category)))

      (train, new EmptyEvaluationInfo, test)
    }
  }

}

/** Observation class serving as a wrapper for both our
  * data's class label and document string.
  */
case class Observation(
  label: Double,
  text: String,
  category: String
) extends Serializable

/** TrainingData class serving as a wrapper for all
  * read in from the Event Server.
  */
class TrainingData(
  val data : RDD[Observation],
  val stopWords : Set[String]
) extends Serializable with SanityCheck {

  /** Sanity check to make sure your data is being fed in correctly. */
  def sanityCheck(): Unit = {
    try {
      val obs : Array[Double] = data.takeSample(false, 5).map(_.label)

      println()
      (0 until 5).foreach(
        k => println("Observation " + (k + 1) +" label: " + obs(k))
      )
      println()
    } catch {
      case (e : ArrayIndexOutOfBoundsException) => {
        println()
        println("Data set is empty, make sure event fields match imported data.")
        println()
      }
    }

  }

}